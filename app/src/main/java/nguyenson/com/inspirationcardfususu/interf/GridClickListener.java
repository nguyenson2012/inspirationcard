package nguyenson.com.inspirationcardfususu.interf;

import android.view.View;

/**
 * Created by Administrator on 03/09/2017.
 */

public interface GridClickListener {
    public void onClick(View itemView, int position);
}
