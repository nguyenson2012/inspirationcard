package nguyenson.com.inspirationcardfususu.fragment;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.util.CommonUtil;
import nguyenson.com.inspirationcardfususu.util.MyPreference;

/**
 * Created by prdcv172 on 4/5/18.
 */

public class FragmentDailyReminder extends Fragment implements View.OnClickListener{
    public static final String TAG = "FragmentDailyReminder";
    private TextView mTextviewAlarmState;
    private Switch mSwichEnableAlarm;
    private TextView mTextviewTimeReminderDetail;
    private ImageView mImageEditTime;

    private MyPreference mMyPreference;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_enable_alarm,container,false);
        setupView(rootView);
        registerEvent();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMyPreference = new MyPreference(getActivity());
        if(mMyPreference.isAlarmAvailable()){
            mSwichEnableAlarm.setChecked(true);
        }else {
            mSwichEnableAlarm.setChecked(false);
        }
        showCurrentTimeReminder();
    }

    private void showCurrentTimeReminder() {
        int selectedHour = mMyPreference.getHourMorning();
        int selectedMinutes = mMyPreference.getMinuteMorning();
        if(selectedHour == 0){
            selectedHour = 8;
        }
        mTextviewTimeReminderDetail.setText(CommonUtil.convertToTwoDigitNumber(selectedHour) + ":"
                +CommonUtil.convertToTwoDigitNumber(selectedMinutes));
    }

    private void registerEvent() {
        mSwichEnableAlarm.setOnClickListener(this);
        mTextviewTimeReminderDetail.setOnClickListener(this);
        mImageEditTime.setOnClickListener(this);
    }

    private void setupView(View rootView) {
        mTextviewAlarmState = (TextView)rootView.findViewById(R.id.alarm_tv_reminder_setting);
        mSwichEnableAlarm = (Switch) rootView.findViewById(R.id.alarm_reminder_switch);
        mTextviewTimeReminderDetail = (TextView)rootView.findViewById(R.id.alarm_tv_reminder_time);
        mImageEditTime = (ImageView)rootView.findViewById(R.id.alarm_img_edit_time);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.alarm_reminder_switch:
                if(mSwichEnableAlarm.isChecked()){
                    mTextviewAlarmState.setText(getString(R.string.reminder_is_on));
                    mMyPreference.setAlarmAvailable(true);
                }else {
                    mTextviewAlarmState.setText(getString(R.string.reminder_is_off));
                    mMyPreference.setAlarmAvailable(false);
                }
                break;
            case R.id.alarm_tv_reminder_time:
                showDialogChooseReminderTime();
                break;
            case R.id.alarm_img_edit_time:
                showDialogChooseReminderTime();
                break;
        }
    }

    private void showDialogChooseReminderTime() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                mTextviewTimeReminderDetail.setText(CommonUtil.convertToTwoDigitNumber(selectedHour) + ":"
                        +CommonUtil.convertToTwoDigitNumber(selectedMinute));
                mMyPreference.putMorningTimeAlarm(selectedHour,selectedMinute);
                mMyPreference.setStateChangeTimeAlarm();
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle(getString(R.string.choose_time_alarm));
        mTimePicker.show();
    }
}
