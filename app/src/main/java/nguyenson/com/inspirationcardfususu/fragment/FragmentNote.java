package nguyenson.com.inspirationcardfususu.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.adapter.ImageNoteAdapter;
import nguyenson.com.inspirationcardfususu.database.DatabaseManager;
import nguyenson.com.inspirationcardfususu.database.DumpData;
import nguyenson.com.inspirationcardfususu.interf.GridClickListener;
import nguyenson.com.inspirationcardfususu.model.ImageInfo;
import nguyenson.com.inspirationcardfususu.util.BitmapTransform;
import nguyenson.com.inspirationcardfususu.util.CommonUtil;
import nguyenson.com.inspirationcardfususu.util.Const;
import nguyenson.com.inspirationcardfususu.util.MyPreference;

/**
 * Created by nguyen on 25/04/2018.
 */

public class FragmentNote extends Fragment implements GridClickListener {
    private RecyclerView mRecyclerViewNote;
    private TextView mTextviewNoImageNoted;
    public static final String TAG = "FragmentNote";

    private ImageNoteAdapter imageNoteAdapter;
    private List<ImageInfo> listImageNoted;

    private static final int MAX_WIDTH = CommonUtil.getScreenWidth();
    private static final int MAX_HEIGHT = (int) (CommonUtil.getScreenHeight()/4);

    int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_note,container,false);
        mRecyclerViewNote = (RecyclerView)rootView.findViewById(R.id.noted_recyclerview);
        mTextviewNoImageNoted =(TextView)rootView.findViewById(R.id.noted_tv_have_no_image_note);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listImageNoted = getNotedIImageID();
        imageNoteAdapter = new ImageNoteAdapter(getActivity(),listImageNoted);
        imageNoteAdapter.setListener(this);
        mRecyclerViewNote.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerViewNote.setHasFixedSize(true);

        mRecyclerViewNote.setAdapter(imageNoteAdapter);

        if(listImageNoted.size() == 0){
            mTextviewNoImageNoted.setVisibility(View.VISIBLE);
            mRecyclerViewNote.setVisibility(View.GONE);
        }else {
            mTextviewNoImageNoted.setVisibility(View.GONE);
            mRecyclerViewNote.setVisibility(View.VISIBLE);
        }
    }

    private List<ImageInfo> getNotedIImageID() {
        List<ImageInfo> listImageInfo = new ArrayList<>();
        MyPreference myPreference = new MyPreference(getActivity());
        String combineImageID = myPreference.getStringObject(Const.KEY_SAVE_COMBINE_NOTED_IMAGE_ID);
        String[] imageIDs = combineImageID.split("-");
        for(String imageID : imageIDs){
            if(imageID.length() != 0){
                ImageInfo imageInfo = DatabaseManager.getInstance().getCardByID(Integer.parseInt(imageID));
                if(imageInfo != null){
                    listImageInfo.add(imageInfo);
                }
            }
        }
        return listImageInfo;
    }

    @Override
    public void onClick(View itemView, int position) {
        openDialogImageDetail(position);
    }

    private void openDialogImageDetail(int cardPosition){
        List<Integer> allImageID = DumpData.getAllImageLocal(getActivity());
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_image_detail);

        dialog.setTitle(getString(R.string.edit_note));

        final ImageView imageViewDetail = (ImageView) dialog.findViewById(R.id.dialog_image_detail);
        final EditText textViewImageNoted = (EditText) dialog.findViewById(R.id.dialog_tv_image_note);
        TextView textViewOK = (TextView) dialog.findViewById(R.id.dialog_tv_ok);
        TextView textViewCancel =(TextView) dialog.findViewById(R.id.dialog_tv_cancel);

        final ImageInfo imageInfo = listImageNoted.get(cardPosition);
        Picasso.with(getActivity()).load(allImageID.get(imageInfo.getmImageID()-1))
                .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                .skipMemoryCache()
                .resize(size, size)
                .centerInside()
                .into(imageViewDetail, new Callback() {
                    @Override
                    public void onSuccess() {
                        textViewImageNoted.setText(imageInfo.getNoteForImage());
                    }

                    @Override
                    public void onError() {
                        imageViewDetail.setImageResource(R.drawable.no_image);
                    }
                });
        textViewOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //update Note
                if(!textViewImageNoted.getText().toString().equals(imageInfo.getNoteForImage())){
                    ImageInfo newImageInfo = new ImageInfo(imageInfo.getmImageID(),textViewImageNoted.getText().toString());
                    DatabaseManager.getInstance().createImageQuote(newImageInfo);
                    reloadRecyclerView();
                }
                dialog.dismiss();
            }
        });
        textViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    private void reloadRecyclerView() {
        listImageNoted = getNotedIImageID();
        imageNoteAdapter.notifyDataSetChanged();
    }
}
