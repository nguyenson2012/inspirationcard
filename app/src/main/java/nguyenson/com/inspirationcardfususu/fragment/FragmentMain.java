package nguyenson.com.inspirationcardfususu.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.adapter.ImageGridAdapter;
import nguyenson.com.inspirationcardfususu.database.DumpData;
import nguyenson.com.inspirationcardfususu.interf.GridClickListener;
import nguyenson.com.inspirationcardfususu.util.Const;
import nguyenson.com.inspirationcardfususu.util.ScreenManager;

/**
 * Created by Administrator on 10/09/2017.
 */

public class FragmentMain extends Fragment {
    public static String TAG="FragmentMain";
    private RecyclerView mRecyclerViewImage;
    private ImageGridAdapter mImageGridAdapter;
    private List<Integer> mListImage;

    private ScreenManager screenManager;

    private FragmentCardInViewPager fragmentCardInViewPager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_main, container, false);
        setupView(layout);
        return layout;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        setAdapterForRecyclerView();
    }

    private void init() {
        screenManager=ScreenManager.getInst();
        fragmentCardInViewPager=new FragmentCardInViewPager();
    }

    private void setAdapterForRecyclerView() {
        mListImage= DumpData.getAllImageLocal(getActivity());
        mImageGridAdapter=new ImageGridAdapter(getActivity(),mListImage);
        mImageGridAdapter.setListener(new GridClickListener() {
            @Override
            public void onClick(View itemView, int position) {
                openDetailImage(position);
            }
        });
        mRecyclerViewImage.setLayoutManager(new GridLayoutManager(getActivity(),3));
        mRecyclerViewImage.setHasFixedSize(true);
        mRecyclerViewImage.setAdapter(mImageGridAdapter);
    }

    private void openDetailImage(int position) {
        Bundle argument=new Bundle();
        argument.putInt(Const.POSITION_ITEM_CLICK,position);
        fragmentCardInViewPager.setArguments(argument);
        screenManager.openFragmentWithAnimation(getFragmentManager(),R.id.fragment_container,fragmentCardInViewPager,true,FragmentCardInViewPager.TAG);
    }

    private void setupView(View layout) {
        mRecyclerViewImage=(RecyclerView)layout.findViewById(R.id.recyclerview);
    }
}
