package nguyenson.com.inspirationcardfususu.fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.util.Const;
import nguyenson.com.inspirationcardfususu.util.MyPreference;

/**
 * Created by Administrator on 05/04/2018.
 */

public class FragmentAboutApp extends Fragment implements View.OnClickListener{
    public static final String TAG = "FragmentAboutApp";
    private RelativeLayout layoutAuthor;
    private RelativeLayout layoutReviewApp;
    private TextView textViewAppVersion;
    private TextView textViewGoWebsite;
    private RelativeLayout layoutCheckoutAndGetAllCard;

    private MyPreference myPreference;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about,container,false);
        setupView(rootView);
        registerEvent();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myPreference = new MyPreference(getActivity());
        showAppVersion();
        blink();
    }

    private void registerEvent() {
        layoutAuthor.setOnClickListener(this);
        layoutReviewApp.setOnClickListener(this);
        layoutCheckoutAndGetAllCard.setOnClickListener(this);
    }

    private void setupView(View rootView) {
        layoutAuthor = (RelativeLayout) rootView.findViewById(R.id.about_layout_author);
        layoutReviewApp = (RelativeLayout) rootView.findViewById(R.id.about_layout_review_app);
        textViewAppVersion = (TextView) rootView.findViewById(R.id.about_tv_app_version);
        layoutCheckoutAndGetAllCard = (RelativeLayout) rootView.findViewById(R.id.about_layout_get_full_card);
        textViewGoWebsite = (TextView) layoutCheckoutAndGetAllCard.findViewById(R.id.about_tv_get_full_card_label);
    }

    private void showAppVersion() {
        PackageManager packageManager = getActivity().getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getActivity().getPackageName(),
                    PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (null != packageInfo && null != packageInfo.versionName) {
            int index = packageInfo.versionName.lastIndexOf(".");
            String ver = packageInfo.versionName.substring(0, index);
            String build = packageInfo.versionName.substring(index + 1);

            textViewAppVersion.setText(packageInfo.versionName+"");
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.about_layout_author:
                openFususuWebsiteLink();
                break;
            case R.id.about_layout_review_app:
                rateApp();
                break;
            case R.id.about_layout_get_full_card:
                openAffilateLink();
                break;
        }
    }

    private void blink(){
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 600;    //in milissegunds
                try{Thread.sleep(timeToBlink);}catch (Exception e) {}
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        if(textViewGoWebsite.getVisibility() == View.VISIBLE){
                            textViewGoWebsite.setVisibility(View.INVISIBLE);
                        }else{
                            textViewGoWebsite.setVisibility(View.VISIBLE);
                        }
                        blink();
                    }
                });
            }
        }).start();
    }

    private void openAffilateLink() {
        String url = getString(R.string.affilate_link);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void openFususuWebsiteLink() {
        String url = getString(R.string.fususu_link);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void rateApp() {
        myPreference.saveBooleanObject(Const.KEY_DONE_RATE_APP,true);
        try {
            Intent rateIntent = new Intent(Intent.ACTION_VIEW);
            rateIntent.setData(Uri.parse(getString(R.string.app_market_id)
                    + getActivity().getPackageName()));
            startActivity(rateIntent);
        } catch (Exception e) {
            Toast.makeText(getActivity(),getString(R.string.error_happen),Toast.LENGTH_SHORT).show();
        }
    }
}
