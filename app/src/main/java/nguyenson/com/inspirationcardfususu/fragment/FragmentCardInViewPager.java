package nguyenson.com.inspirationcardfususu.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.adapter.ImageViewPagerAdapter;
import nguyenson.com.inspirationcardfususu.database.DumpData;
import nguyenson.com.inspirationcardfususu.transformer.DrawBackTransformer;
import nguyenson.com.inspirationcardfususu.util.Const;

/**
 * Created by Administrator on 10/09/2017.
 */

public class FragmentCardInViewPager extends Fragment {
    public static String TAG="FragmentCardInViewPager";
    private ViewPager viewPagerAllCard;
    private int positionItemClick=0;
    private List<Integer> listImage;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_image_in_viewpager, container, false);
        setupView(layout);
        return layout;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getPositionOfImageDetail();
        setAdapterForViewPager();

    }

    private void getPositionOfImageDetail() {
        Bundle argument=getArguments();
        if(argument!=null){
            positionItemClick=argument.getInt(Const.POSITION_ITEM_CLICK);
        }
    }

    private void setAdapterForViewPager() {
        listImage= DumpData.getAllImageLocal(getActivity());
        ImageViewPagerAdapter imageViewPagerAdapter=new ImageViewPagerAdapter(listImage,getActivity());
        viewPagerAllCard.setAdapter(imageViewPagerAdapter);
        viewPagerAllCard.setCurrentItem(positionItemClick);
        viewPagerAllCard.setPageTransformer(true,new DrawBackTransformer());
    }

    private void setupView(View layout) {
        viewPagerAllCard=(ViewPager)layout.findViewById(R.id.view_pager_image);
    }
}
