package nguyenson.com.inspirationcardfususu.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.adapter.NavigationItemAdapter;
import nguyenson.com.inspirationcardfususu.model.NavigationItem;

/**
 * Created by Administrator on 10/09/2017.
 */

public class FragmentDrawer extends Fragment {
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View containerView;
    private OnItemNavDrawerClickInterface drawerClickListener;

    private ListView listViewNavigationItem;
    private List<NavigationItem> listNavigationItem;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        setupView(layout);
        return layout;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setAdapterForListviewNavigationItem();
    }

    private void setAdapterForListviewNavigationItem() {
        listNavigationItem=new ArrayList<NavigationItem>();
        NavigationItem allImageItem=new NavigationItem(R.drawable.image_area,R.string.home);
        listNavigationItem.add(allImageItem);
        NavigationItem choosenTimeItem=new NavigationItem(R.drawable.alarm,R.string.choose_time);
        listNavigationItem.add(choosenTimeItem);
        NavigationItem infoItem=new NavigationItem(R.drawable.information_green,R.string.info_app);
        listNavigationItem.add(infoItem);
        NavigationItemAdapter adapter=new NavigationItemAdapter(getActivity(),R.layout.item_navigation,listNavigationItem);
        listViewNavigationItem.setAdapter(adapter);

        listViewNavigationItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                drawerClickListener.onDrawerItemSelected(view,position);
                mDrawerLayout.closeDrawer(containerView);
            }
        });
    }

    private void setupView(View layout) {
        listViewNavigationItem=(ListView)layout.findViewById(R.id.listview_item_nav);
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    public void setDrawerClickListener(OnItemNavDrawerClickInterface drawerClickListener) {
        this.drawerClickListener = drawerClickListener;
    }

    public void setDrawerState(boolean isEnabled) {
        if ( isEnabled ) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            mDrawerToggle.onDrawerStateChanged(DrawerLayout.LOCK_MODE_UNLOCKED);
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            mDrawerToggle.syncState();

        }
        else {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mDrawerToggle.onDrawerStateChanged(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            mDrawerToggle.syncState();
        }
    }
    public interface OnItemNavDrawerClickInterface{
        public void onDrawerItemSelected(View view, int position);
    }
}
