package nguyenson.com.inspirationcardfususu.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.database.DatabaseManager;
import nguyenson.com.inspirationcardfususu.database.DumpData;
import nguyenson.com.inspirationcardfususu.model.ImageInfo;
import nguyenson.com.inspirationcardfususu.util.BitmapTransform;
import nguyenson.com.inspirationcardfususu.util.CommonUtil;
import nguyenson.com.inspirationcardfususu.util.Const;
import nguyenson.com.inspirationcardfususu.util.MyPreference;
import nguyenson.com.inspirationcardfususu.util.ShakeDetector;

/**
 * Created by Administrator on 10/09/2017.
 */

public class FragmentDetailCard extends Fragment implements ShakeDetector.Listener,View.OnClickListener{
    public static String TAG="FragmentDetailCard";
    private int positionImage;
    private ImageView imageViewDetail;
    private ImageView imageViewDropShadow;
    private LinearLayout mLayoutShare;
    private ImageView imageShare;
    private ImageView mImageCellPhoneShake;
    private TextView mTextViewInstruction;
    private TextView mTextviewShareLove;
    private LinearLayout mlayoutAddNote;
    private ImageView mImageAddNote;
    private TextView mTextviewAddNote;
    private EditText mTextviewNoteForImage;
    private ConstraintLayout mScrollViewNote;
    private TextView mTextviewSaveNote;
    private ImageView mImageViewSave;
    private TextView mTextviewGetCardprinted;
    private AVLoadingIndicatorView mProgressBar;
    private List<Integer> allImage;

    private static final int MAX_WIDTH = CommonUtil.getScreenWidth();
    private static final int MAX_HEIGHT = (int) (CommonUtil.getScreenHeight() * 3 /4);

    int size = (int) ( 5 * Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT)));
    private boolean isBlink = false;
    private int numberShakePhone = 0;

    private MyPreference myPreference;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_detail_card, container, false);
        setupView(layout);
        registerEvent();
        //initGesture(layout);
        return layout;
    }

    private void registerEvent() {
        imageShare.setOnClickListener(this);
        mTextviewShareLove.setOnClickListener(this);
        mScrollViewNote.setOnClickListener(this);
        mTextviewSaveNote.setOnClickListener(this);
        mImageViewSave.setOnClickListener(this);
        mTextviewGetCardprinted.setOnClickListener(this);
    }

    private void displayRandomImage() {
        mTextviewNoteForImage.setText("");
        Random random=new Random();
        int randomPosition = 0;
        do{
            randomPosition=random.nextInt(allImage.size());
        }while (randomPosition == positionImage);//make sure next image is different with previous image

        positionImage = randomPosition;
        ImageInfo imageInfo = DatabaseManager.getInstance().getCardByID(positionImage+1);
        mTextviewNoteForImage.setText(imageInfo.getNoteForImage());

        Picasso.with(getActivity()).load(allImage.get(randomPosition))
                .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                .skipMemoryCache()
                .resize(size, size)
                .centerInside()
                .into(imageViewDetail, new Callback() {
            @Override
            public void onSuccess() {
                imageViewDropShadow.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                mProgressBar.setVisibility(View.GONE);
                imageViewDetail.setImageResource(R.drawable.no_image);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myPreference = new MyPreference(getActivity());
        allImage=DumpData.getAllImageLocal(getActivity());
        //showImageDetail();
        showBackCardFirst();
        SensorManager sensorManager = (SensorManager) getActivity().getSystemService(getActivity().SENSOR_SERVICE);
        ShakeDetector sd = new ShakeDetector(this);
        sd.start(sensorManager);

        Animation animation = AnimationUtils.loadAnimation(getActivity(),R.anim.shake);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mImageCellPhoneShake.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mImageCellPhoneShake.startAnimation(animation);

    }

    private void showBackCardFirst() {
        Picasso.with(getActivity()).load(R.drawable.susucard_back)
                .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                .skipMemoryCache()
                .resize(size, size)
                .centerInside()
                .into(imageViewDetail, new Callback() {
            @Override
            public void onSuccess() {
                mProgressBar.setVisibility(View.GONE);
                imageViewDropShadow.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {
                mProgressBar.setVisibility(View.GONE);
                imageViewDetail.setImageResource(R.drawable.no_image);
            }
        });
    }

    private void setupView(View layout) {
        imageViewDetail=(ImageView)layout.findViewById(R.id.image_detail);
        imageViewDropShadow = (ImageView)layout.findViewById(R.id.image_drop_shadow);
        mImageCellPhoneShake = (ImageView)layout.findViewById(R.id.main_image_cellphone);
        mTextViewInstruction = (TextView)layout.findViewById(R.id.main_tv_instruction_shake_phone);
        mProgressBar=(AVLoadingIndicatorView) layout.findViewById(R.id.progress_refresh);
        imageShare = (ImageView)layout.findViewById(R.id.image_share);
        mTextviewShareLove = (TextView)layout.findViewById(R.id.textview_share_image);
        mImageAddNote = (ImageView)layout.findViewById(R.id.main_image_add_note);
        mTextviewAddNote = (TextView)layout.findViewById(R.id.main_tv_add_note);
        mTextviewNoteForImage = (EditText) layout.findViewById(R.id.main_tv_note_for_image);
        mScrollViewNote = (ConstraintLayout) layout.findViewById(R.id.main_scrollview);
        mTextviewSaveNote = (TextView) layout.findViewById(R.id.main_tv_save_note);
        mLayoutShare = (LinearLayout) layout.findViewById(R.id.main_layout_share);
        mlayoutAddNote = (LinearLayout) layout.findViewById(R.id.main_layout_edit);
        mImageViewSave = (ImageView) layout.findViewById(R.id.main_image_save_note);
        mTextviewGetCardprinted = (TextView) layout.findViewById(R.id.maint_tv_get_card_printed);
    }

    private void takeScreenShortAndShare() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(getActivity());
                    }
                    builder.setTitle(getString(R.string.information))
                            .setMessage(getString(R.string.need_save_image))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                } else {
                    requestPermissions(
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            Const.MY_PERMISSION_REQUEST_WRITE_EXTERNAL);
                }
            }else {
                captureViewAndShare();
            }
        }else {
            captureViewAndShare();
        }
    }

    private void captureViewAndShare(){
        imageViewDetail.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(imageViewDetail.getDrawingCache());
        imageViewDetail.setDrawingCacheEnabled(false);
        new ScreenShot().execute(bitmap);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Const.MY_PERMISSION_REQUEST_WRITE_EXTERNAL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    captureViewAndShare();
                } else {
                    shareApp();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image_share:
                takeScreenShortAndShare();
                break;
            case R.id.textview_share_image:
                takeScreenShortAndShare();
                break;
            case R.id.main_tv_save_note:
                saveNote();
                break;
            case R.id.main_image_save_note:
                saveNote();
                break;
            case R.id.maint_tv_get_card_printed:
                openAffilateLink();
                break;
        }
    }

    private void openAffilateLink() {
        String url = getString(R.string.affilate_link);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void saveNote() {
        ImageInfo imageInfo = new ImageInfo(positionImage+1,mTextviewNoteForImage.getText().toString());
        DatabaseManager.getInstance().createImageQuote(imageInfo);
        saveCombineNotedImageID((positionImage+1)+"");
        Toast.makeText(getActivity(),getString(R.string.you_have_save_note),Toast.LENGTH_SHORT).show();
    }

    class ScreenShot extends AsyncTask<Bitmap, String, String> {
        File imageFile;
        @Override
        protected String doInBackground(Bitmap... bitmaps) {
            int count;
            try {
                // image naming and path  to include sd card  appending name you choose for file
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                String currentDateandTime = sdf.format(Calendar.getInstance().getTime());
                String mPath = Environment.getExternalStorageDirectory().toString() + "/" +"fususucard"+currentDateandTime+".png";
                imageFile = new File(mPath);

                FileOutputStream outputStream = new FileOutputStream(imageFile);
                int quality = 100;
                bitmaps[0].compress(Bitmap.CompressFormat.PNG, quality, outputStream);
                outputStream.flush();
                outputStream.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }
        @Override
        protected void onPostExecute(String file_url) {
            File checkFile=new File(imageFile.getAbsolutePath());
            if(checkFile.exists()) {
                shareResult(imageFile);
            }else {
                shareApp();
            }
        }
    }

    private void blink(){
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 600;    //in milissegunds
                try{Thread.sleep(timeToBlink);}catch (Exception e) {}
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        if(mTextviewGetCardprinted.getVisibility() == View.VISIBLE){
                            mTextviewGetCardprinted.setVisibility(View.INVISIBLE);
                        }else{
                            mTextviewGetCardprinted.setVisibility(View.VISIBLE);
                        }
                        blink();
                    }
                });
            }
        }).start();
    }

    private void shareResult(File imageFile) {
        Intent shareIntent = new Intent();
        Uri photoURI = FileProvider.getUriForFile(getActivity(),
                getActivity().getApplicationContext().getPackageName() + ".nguyenson.com.inspirationcardfususu.util",
                new File(imageFile.getAbsolutePath()));
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_img_message)+"\n"+getString(R.string.go_and_get_fullcard)+"\n"
        +getString(R.string.affilate_link));
        shareIntent.putExtra(Intent.EXTRA_STREAM, photoURI);
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        Intent chooserIntent = Intent.createChooser(shareIntent, getString(R.string.choose_app_to_share));
        startActivity(chooserIntent);
    }

    private void shareApp() {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            share.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        }
        share.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_app_title));
        share.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.link_google_play_for_share));

        startActivity(Intent.createChooser(share, getResources().getString(R.string.share_app_title)));
    }

    @Override
    public void hearShake() {
        if(!isBlink){
            blink();
            isBlink = true;
        }
        numberShakePhone++;
        if(numberShakePhone%5 ==0){
            mTextviewGetCardprinted.setVisibility(View.VISIBLE);
        }
        mLayoutShare.setVisibility(View.VISIBLE);
        mlayoutAddNote.setVisibility(View.VISIBLE);
        mScrollViewNote.setVisibility(View.VISIBLE);
        imageViewDropShadow.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
        mImageCellPhoneShake.clearAnimation();
        mImageCellPhoneShake.setVisibility(View.GONE);
        mTextViewInstruction.setVisibility(View.GONE);
        mImageAddNote.setVisibility(View.VISIBLE);
        mTextviewAddNote.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
        mTextviewShareLove.setVisibility(View.VISIBLE);
        imageShare.setVisibility(View.VISIBLE);
        displayRandomImage();
    }

    private void saveCombineNotedImageID(String imageID) {
        String combineNotedID = myPreference.getStringObject(Const.KEY_SAVE_COMBINE_NOTED_IMAGE_ID);
        myPreference.saveStringObject(Const.KEY_SAVE_COMBINE_NOTED_IMAGE_ID,combineNotedID+"-"+imageID);
    }
}
