package nguyenson.com.inspirationcardfususu.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import nguyenson.com.inspirationcardfususu.activity.SnoozeActivity;
import nguyenson.com.inspirationcardfususu.service.AlarmServiceBroadcastReciever;
import nguyenson.com.inspirationcardfususu.util.MyPreference;
import nguyenson.com.inspirationcardfususu.util.StaticWakeLock;

/**
 * Created by Administrator on 17/09/2017.
 */

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent mathAlarmServiceIntent = new Intent(
                context, AlarmServiceBroadcastReciever.class);
        context.sendBroadcast(mathAlarmServiceIntent, null);

        StaticWakeLock.lockOn(context);

        Intent i = new Intent(context, SnoozeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MyPreference myPreference = new MyPreference(context);
        if(myPreference.isAlarmAvailable()){
            context.startActivity(i);
        }

    }
}
