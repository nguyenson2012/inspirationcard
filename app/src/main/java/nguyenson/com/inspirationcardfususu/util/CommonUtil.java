package nguyenson.com.inspirationcardfususu.util;

import android.content.res.Resources;

/**
 * Created by Administrator on 17/09/2017.
 */

public class CommonUtil {
    public static String convertToTwoDigitNumber(int number){
        if(number<10){
            return "0"+number+"";
        }else {
            return ""+number;
        }
    }

    /**
     * Function is used to get screen width
     *
     * @return screen width
     */
    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    /**
     * Function is used to get screen width
     *
     * @return screen height
     */
    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }
}
