package nguyenson.com.inspirationcardfususu.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Administrator on 17/09/2017.
 */

public class MyPreference {
    private SharedPreferences mPreferences;
    public MyPreference(Context context){
        mPreferences=context.getSharedPreferences(Const.MY_PREFERENCES,Context.MODE_PRIVATE);
    }

    public void putMorningTimeAlarm(int hourMorning,int minuteMorning){
        SharedPreferences.Editor editor=mPreferences.edit();
        editor.putInt(Const.HOUR_MORNING,hourMorning);
        editor.putInt(Const.MINUTE_MORNING,minuteMorning);
        editor.commit();
    }

    public int getHourMorning(){
        return mPreferences.getInt(Const.HOUR_MORNING,0);
    }

    public int getMinuteMorning(){
        return mPreferences.getInt(Const.MINUTE_MORNING,0);
    }

    public void setAlarmAvailable(boolean isAvailable){
        SharedPreferences.Editor editor=mPreferences.edit();
        editor.putBoolean(Const.KEY_ALARM,isAvailable);
        editor.commit();
    }

    public boolean isAlarmAvailable(){
        return mPreferences.getBoolean(Const.KEY_ALARM,true);
    }

    public void setStateChangeTimeAlarm(){
        SharedPreferences.Editor editor=mPreferences.edit();
        editor.putBoolean(Const.KEY_CHANGED_ALARM,true);
        editor.commit();
    }

    public void doneOpenAppFirstTime(){
        SharedPreferences.Editor editor=mPreferences.edit();
        editor.putBoolean(Const.KEY_OPEN_APP,true);
        editor.commit();
    }

    public boolean isOpenAppFirstTime(){
        return mPreferences.getBoolean(Const.KEY_OPEN_APP,false);
    }

    public void saveBooleanObject(String key, boolean value) {
        SharedPreferences.Editor editor=mPreferences.edit();
        editor.putBoolean(key,value);
        editor.commit();
    }

    public boolean getBooleanObject(String key){
        return mPreferences.getBoolean(key,false);
    }

    public void saveStringObject(String key,String value){
        SharedPreferences.Editor editor=mPreferences.edit();
        editor.putString(key,value);
        editor.commit();
    }

    public String getStringObject(String key){
        return mPreferences.getString(key,"");
    }
}
