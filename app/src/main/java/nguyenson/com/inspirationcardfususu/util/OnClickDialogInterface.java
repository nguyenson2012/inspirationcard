package nguyenson.com.inspirationcardfususu.util;

/**
 * Created by Administrator on 02/01/2018.
 */

public interface OnClickDialogInterface {
    void onClickOK();
    void onClickCancel();
}
