package nguyenson.com.inspirationcardfususu.util;

/**
 * Created by Administrator on 09/09/2017.
 */

public class Const {
    public static final String POSITION_ITEM_CLICK="position_item";
    public static final java.lang.String POSITION_ITEM_RANDOM = "position_random";
    public static final String MY_PREFERENCES = "my_pref";
    public static final String HOUR_MORNING = "hour_morning";
    public static final String MINUTE_MORNING = "minute_morning";
    public static final String KEY_ALARM = "alarm";
    public static final int MORNING_REQUEST_CODE = 0;
    public static final String KEY_CHANGED_ALARM = "KEY_CHANGED_ALARM";
    public static final String KEY_OPEN_APP = "KEY_OPEN_APP";
    public static final String KEY_DONE_RATE_APP = "KEY_DONE_RATE_APP";
    public static final int MY_PERMISSION_REQUEST_WRITE_EXTERNAL = 1000;
    public static final String KEY_DONE_SAVE_DATABASE = "KEY_DONE_SAVE_DATABASE";
    public static final String KEY_SAVE_COMBINE_NOTED_IMAGE_ID = "KEY_SAVE_COMBINE_NOTED_IMAGE_ID";
}
