package nguyenson.com.inspirationcardfususu.database;

import java.util.List;

import io.realm.Realm;
import nguyenson.com.inspirationcardfususu.model.ImageInfo;

/**
 * Created by Administrator on 03/09/2017.
 */

public class DatabaseManager {
    private Realm mRealm;
    private static DatabaseManager mInstance;

    private DatabaseManager(){
        mRealm=Realm.getDefaultInstance();
    }

    public static DatabaseManager getInstance(){
        if(mInstance==null){
            mInstance=new DatabaseManager();
        }
        return mInstance;
    }

    public void createImageQuote(ImageInfo imageInfo){
        mRealm.beginTransaction();
        mRealm.copyToRealmOrUpdate(imageInfo);
        mRealm.commitTransaction();
    }

    public List<ImageInfo> getAllCard(){
        return mRealm.where(ImageInfo.class).findAll();
    }

    public ImageInfo getCardByID(int cardID){
        return mRealm.where(ImageInfo.class).equalTo("mImageID",cardID).findFirst();
    }
}
