package nguyenson.com.inspirationcardfususu.database;

import android.content.Context;
import android.content.res.Resources;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 03/09/2017.
 */

public class DumpData {

    public static List<Integer> getAllImageLocal(Context context){
        List<Integer> listAllImage=new ArrayList<Integer>();
        Resources resources = context.getResources();
        for(int i=1;i<=156;i++) {
            final int resourceId = resources.getIdentifier("s" +i, "drawable",
                    context.getPackageName());
            listAllImage.add(resourceId);
        }
        return listAllImage;
    }
}
