package nguyenson.com.inspirationcardfususu.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.adapter.SplashPagerAdapter;
import nguyenson.com.inspirationcardfususu.database.DatabaseManager;
import nguyenson.com.inspirationcardfususu.model.ImageInfo;
import nguyenson.com.inspirationcardfususu.model.PagerInfo;
import nguyenson.com.inspirationcardfususu.util.Const;
import nguyenson.com.inspirationcardfususu.util.MyPreference;

/**
 * Created by nguyen on 17/04/2018.
 */

public class SplashActivity extends Activity {
    private ViewPager mViewPager;
    private CircleIndicator indicator;
    private TextView mTextviewStart;

    private List<PagerInfo> mListPage;

    private MyPreference myPreference;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mViewPager = (ViewPager) findViewById(R.id.splash_viewpager);
        indicator = (CircleIndicator)findViewById(R.id.indicator);
        mTextviewStart = (TextView)findViewById(R.id.splash_tv_start);

        myPreference = new MyPreference(SplashActivity.this);

        if(!myPreference.getBooleanObject(Const.KEY_DONE_SAVE_DATABASE)){
            addDefaultCardDatabase();
        }
        setDefaultSplashPager();
        SplashPagerAdapter splashPagerAdapter = new SplashPagerAdapter(mListPage,SplashActivity.this);
        mViewPager.setAdapter(splashPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 2){
                    indicator.setVisibility(View.GONE);
                    mTextviewStart.setVisibility(View.VISIBLE);
                }else {
                    indicator.setVisibility(View.VISIBLE);
                    mTextviewStart.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        indicator.setViewPager(mViewPager);

        mTextviewStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myPreference.doneOpenAppFirstTime();
                openMainActivity();
            }
        });

        if(myPreference.isOpenAppFirstTime()){
            openMainActivity();
        }
    }

    private void addDefaultCardDatabase() {
        for(int i = 1;i<=104;i++){
            ImageInfo imageInfo = new ImageInfo(i,"");
            DatabaseManager.getInstance().createImageQuote(imageInfo);
        }
        myPreference.saveBooleanObject(Const.KEY_DONE_SAVE_DATABASE,true);
    }

    private void openMainActivity(){
        Intent intent = new Intent(SplashActivity.this,TabActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.trans_in, R.anim.trans_out);
    }

    private void setDefaultSplashPager() {
        mListPage = new ArrayList<>();
        PagerInfo pagerInfoOne = new PagerInfo(R.drawable.logo_fususu,getString(R.string.description_one));
        mListPage.add(pagerInfoOne);
        PagerInfo pagerInfoTwo = new PagerInfo(R.drawable.logo_fususu,getString(R.string.description_two));
        mListPage.add(pagerInfoTwo);
        PagerInfo pagerInfoThree = new PagerInfo(R.drawable.logo_fususu,getString(R.string.description_three));
        mListPage.add(pagerInfoThree);
    }


}
