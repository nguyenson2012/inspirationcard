package nguyenson.com.inspirationcardfususu.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.database.DumpData;
import nguyenson.com.inspirationcardfususu.util.BitmapTransform;
import nguyenson.com.inspirationcardfususu.util.CommonUtil;
import nguyenson.com.inspirationcardfususu.util.StaticWakeLock;

/**
 * Created by Administrator on 17/09/2017.
 */

public class SnoozeActivity extends Activity {
    private ImageView mImageViewSnooze;
    private Vibrator mVibrator;

    private static final int MAX_WIDTH = CommonUtil.getScreenWidth();
    private static final int MAX_HEIGHT = (int) (CommonUtil.getScreenHeight() * 3 /4);
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snooze);
        final Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        mImageViewSnooze=(ImageView)findViewById(R.id.image_snooze);
        List<Integer> allImage= DumpData.getAllImageLocal(getApplicationContext());
        Random random=new Random();
        int randomPosition=random.nextInt(allImage.size());
        Picasso.with(SnoozeActivity.this).load(allImage.get(randomPosition))
                .placeholder(R.drawable.loading)
                .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                .skipMemoryCache()
                .resize(MAX_WIDTH, MAX_HEIGHT)
                .centerInside()
                .into(mImageViewSnooze);
        mVibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        long[] pattern = { 1000, 200, 200, 200 };
        mVibrator.vibrate(pattern, 0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        StaticWakeLock.lockOff(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (mVibrator != null)
                mVibrator.cancel();
        } catch (Exception e) {
        }
    }
}
