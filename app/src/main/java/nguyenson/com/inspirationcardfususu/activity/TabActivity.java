package nguyenson.com.inspirationcardfususu.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import java.util.Calendar;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.fragment.FragmentAboutApp;
import nguyenson.com.inspirationcardfususu.fragment.FragmentDailyReminder;
import nguyenson.com.inspirationcardfususu.fragment.FragmentDetailCard;
import nguyenson.com.inspirationcardfususu.fragment.FragmentNote;
import nguyenson.com.inspirationcardfususu.receiver.AlarmReceiver;
import nguyenson.com.inspirationcardfususu.util.Const;
import nguyenson.com.inspirationcardfususu.util.MyPreference;
import nguyenson.com.inspirationcardfususu.util.OnClickDialogInterface;
import nguyenson.com.inspirationcardfususu.util.ScreenManager;

/**
 * Created by Administrator on 24/04/2018.
 */

public class TabActivity extends AppCompatActivity {
    private AHBottomNavigation ahBottomNavigation;

    private MyPreference mMyPreference;
    private AlarmManager mAlarmManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
        mMyPreference=new MyPreference(this);
        mAlarmManager=(AlarmManager) getSystemService(Context.ALARM_SERVICE);
        ahBottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_bar_navigation);
        setupBottomBar();
        ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.tab_act_frame_container,
                new FragmentDetailCard(),false,FragmentDetailCard.TAG);
    }

    private void setupBottomBar() {
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_1, R.drawable.image_area, R.color.colorPrimary);
        AHBottomNavigationItem itemEdit = new AHBottomNavigationItem(R.string.note,R.drawable.pencil_blue,R.color.colorPrimary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.alarm, R.color.colorPrimary);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.information_green, R.color.colorPrimary);

// Add items
        ahBottomNavigation.addItem(item1);
        ahBottomNavigation.addItem(itemEdit);
        ahBottomNavigation.addItem(item2);
        ahBottomNavigation.addItem(item3);

        ahBottomNavigation.setColored(true);
        ahBottomNavigation.setColoredModeColors(Color.GREEN,Color.WHITE);

        ahBottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if(ahBottomNavigation.getCurrentItem() == position){
                    return true;
                }else {
                    switch (position){
                        case 0:
                            ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.tab_act_frame_container,
                                    new FragmentDetailCard(),false,FragmentDetailCard.TAG);
                            break;
                        case 1:
                            ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.tab_act_frame_container,
                                    new FragmentNote(),false,FragmentNote.TAG);
                            break;
                        case 2:
                            ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.tab_act_frame_container,
                                    new FragmentDailyReminder(),false,FragmentDailyReminder.TAG);
                            break;
                        case 3:
                            ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.tab_act_frame_container,
                                    new FragmentAboutApp(),false,FragmentAboutApp.TAG);
                            break;
                    }
                    return true;
                }

            }
        });
        ahBottomNavigation.setCurrentItem(0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        setupAlarm(mMyPreference.getHourMorning(),mMyPreference.getMinuteMorning(), Const.MORNING_REQUEST_CODE);
    }

    private void setupAlarm(int selectedHour,int selectedMinute,int requestCode) {
        Calendar calendar=Calendar.getInstance();
        int currentHour=calendar.get(Calendar.HOUR_OF_DAY);
        int currentMinutes=calendar.get(Calendar.MINUTE);
        int currentDay=calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
        calendar.set(Calendar.MINUTE, selectedMinute);
        if(selectedHour<currentHour ||(selectedHour==currentHour && selectedMinute< currentMinutes)) {
            calendar.set(Calendar.DAY_OF_MONTH,currentDay+1);
        }

        Intent alarmIntent = new Intent(TabActivity.this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(TabActivity.this, requestCode,
                alarmIntent, 0);
        mAlarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    @Override
    public void onBackPressed() {
        ScreenManager.getInst().showDialog(TabActivity.this, "", getString(R.string.want_exit_app),
                new OnClickDialogInterface() {
                    @Override
                    public void onClickOK() {
                        TabActivity.this.finish();
                    }

                    @Override
                    public void onClickCancel() {

                    }
                });
    }
}
