package nguyenson.com.inspirationcardfususu.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.Calendar;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.fragment.FragmentAboutApp;
import nguyenson.com.inspirationcardfususu.fragment.FragmentDailyReminder;
import nguyenson.com.inspirationcardfususu.fragment.FragmentDetailCard;
import nguyenson.com.inspirationcardfususu.fragment.FragmentDrawer;
import nguyenson.com.inspirationcardfususu.fragment.FragmentMain;
import nguyenson.com.inspirationcardfususu.receiver.AlarmReceiver;
import nguyenson.com.inspirationcardfususu.util.Const;
import nguyenson.com.inspirationcardfususu.util.MyPreference;
import nguyenson.com.inspirationcardfususu.util.OnClickDialogInterface;
import nguyenson.com.inspirationcardfususu.util.ScreenManager;

/**
 * Created by Administrator on 10/09/2017.
 */

public class MainActivity  extends AppCompatActivity implements FragmentDrawer.OnItemNavDrawerClickInterface{
    private FragmentManager mFragmentManager;
    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    private FragmentMain fragmentMain;
    private FragmentDailyReminder fragmentDailyReminder;

    private MyPreference mMyPreference;
    private AlarmManager mAlarmManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupView();
        fragmentMain=new FragmentMain();
        fragmentDailyReminder=new FragmentDailyReminder();
        mMyPreference=new MyPreference(this);
        mAlarmManager=(AlarmManager) getSystemService(Context.ALARM_SERVICE);
    }

    private void setupView() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerClickListener(this);
        mFragmentManager=getSupportFragmentManager();
        addMainFragment();
    }

    private void addMainFragment() {
        FragmentDetailCard fragmentDetailCard=new FragmentDetailCard();
        ScreenManager.getInst().openFragmentWithAnimation(mFragmentManager,R.id.fragment_container,
                fragmentDetailCard,false,FragmentDetailCard.TAG);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        switch (position){
            case 0:
                for(int i=0;i<=getSupportFragmentManager().getBackStackEntryCount();i++){
                    getSupportFragmentManager().popBackStack();
                }
                ScreenManager.getInst().openFragmentWithAnimation(mFragmentManager,R.id.fragment_container,
                        new FragmentDetailCard(),false,FragmentDetailCard.TAG);
                break;
            case 1:// choose inspiration time
                ScreenManager.getInst().openFragmentWithAnimation(mFragmentManager,R.id.fragment_container,
                        fragmentDailyReminder,true,FragmentDailyReminder.TAG);
                break;
            case 2:// show app info
                ScreenManager.getInst().openFragmentWithAnimation(mFragmentManager,R.id.fragment_container,new FragmentAboutApp(),
                        true,null);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        if(isFragmentDetailVisible()){
            openDrawerLayout();
            ScreenManager.getInst().showDialog(MainActivity.this, "", getString(R.string.want_exit_app),
                    new OnClickDialogInterface() {
                        @Override
                        public void onClickOK() {
                            MainActivity.this.finish();
                        }

                        @Override
                        public void onClickCancel() {

                        }
                    });
        }else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        setupAlarm(mMyPreference.getHourMorning(),mMyPreference.getMinuteMorning(),Const.MORNING_REQUEST_CODE);
    }

    private boolean isFragmentDetailVisible() {
        FragmentDetailCard fragmentDetailCard=(FragmentDetailCard)mFragmentManager.findFragmentByTag(FragmentDetailCard.TAG);
        if(fragmentDetailCard==null || !fragmentDetailCard.isVisible()){
            return false;
        }else {
            return true;
        }
    }

    private void setupAlarm(int selectedHour,int selectedMinute,int requestCode) {
        Calendar calendar=Calendar.getInstance();
        int currentHour=calendar.get(Calendar.HOUR_OF_DAY);
        int currentMinutes=calendar.get(Calendar.MINUTE);
        int currentDay=calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
        calendar.set(Calendar.MINUTE, selectedMinute);
        if(selectedHour<currentHour ||(selectedHour==currentHour && selectedMinute< currentMinutes)) {
            calendar.set(Calendar.DAY_OF_MONTH,currentDay+1);
        }

        Intent alarmIntent = new Intent(MainActivity.this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, requestCode,
                alarmIntent, 0);
        mAlarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    public void closeDrawerLayout(){
        drawerFragment.setDrawerState(false);
    }

    public void openDrawerLayout(){
        drawerFragment.setDrawerState(true);
    }
}
