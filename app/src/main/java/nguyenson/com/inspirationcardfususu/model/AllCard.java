package nguyenson.com.inspirationcardfususu.model;

import java.util.List;

/**
 * Created by nguyen on 18/04/2018.
 */

public class AllCard {
    private List<CardInfo> images;

    public AllCard() {
    }

    public AllCard(List<CardInfo> images) {
        this.images = images;
    }

    public List<CardInfo> getImages() {
        return images;
    }

    public void setImages(List<CardInfo> images) {
        this.images = images;
    }
}
