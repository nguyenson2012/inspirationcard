package nguyenson.com.inspirationcardfususu.model;

/**
 * Created by nguyen on 17/04/2018.
 */

public class PagerInfo {
    private int imageResource;
    private String description;

    public PagerInfo() {
    }

    public PagerInfo(int imageResource, String description) {
        this.imageResource = imageResource;
        this.description = description;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
