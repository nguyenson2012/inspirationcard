package nguyenson.com.inspirationcardfususu.model;

/**
 * Created by nguyen on 18/04/2018.
 */

public class CardInfo {
    private int id;
    private String link;

    public CardInfo() {
    }

    public CardInfo(int id, String link) {
        this.id = id;
        this.link = link;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
