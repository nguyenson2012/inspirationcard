package nguyenson.com.inspirationcardfususu.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Administrator on 03/09/2017.
 */

public class ImageInfo extends RealmObject{
    @PrimaryKey
    private int mImageID;
    private String noteForImage;

    public ImageInfo() {
    }

    public ImageInfo(int mImageID, String noteForImage) {
        this.mImageID = mImageID;
        this.noteForImage = noteForImage;
    }

    public String getNoteForImage() {
        return noteForImage;
    }

    public void setNoteForImage(String noteForImage) {
        this.noteForImage = noteForImage;
    }

    public int getmImageID() {
        return mImageID;
    }

    public void setmImageID(int mImageID) {
        this.mImageID = mImageID;
    }
}
