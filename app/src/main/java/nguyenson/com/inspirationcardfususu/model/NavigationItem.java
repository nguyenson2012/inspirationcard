package nguyenson.com.inspirationcardfususu.model;

/**
 * Created by Administrator on 10/09/2017.
 */

public class NavigationItem {
    private int mDrawableItem;
    private int mTitle;

    public NavigationItem(int mDrawableItem, int mTitle) {
        this.mDrawableItem = mDrawableItem;
        this.mTitle = mTitle;
    }

    public NavigationItem() {
    }

    public int getDrawableItem() {
        return mDrawableItem;
    }

    public void setDrawableItem(int mDrawableItem) {
        this.mDrawableItem = mDrawableItem;
    }

    public int getTitle() {
        return mTitle;
    }

    public void setTitle(int mTitle) {
        this.mTitle = mTitle;
    }
}
