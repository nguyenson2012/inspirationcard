package nguyenson.com.inspirationcardfususu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.database.DumpData;
import nguyenson.com.inspirationcardfususu.interf.GridClickListener;
import nguyenson.com.inspirationcardfususu.model.ImageInfo;
import nguyenson.com.inspirationcardfususu.util.BitmapTransform;
import nguyenson.com.inspirationcardfususu.util.CommonUtil;

/**
 * Created by nguyen on 25/04/2018.
 */

public class ImageNoteAdapter extends RecyclerView.Adapter<ImageNoteAdapter.MyViewHolder> {
    private LayoutInflater mInflater;
    private Context mContext;
    //
    private List<ImageInfo> mListImageInfo;

    private GridClickListener mListener;

    private static final int MAX_WIDTH = CommonUtil.getScreenWidth();
    private static final int MAX_HEIGHT = (int) (CommonUtil.getScreenHeight() * 3 /4);

    int size = (int) (5 * Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT)));

    public ImageNoteAdapter(Context context, List<ImageInfo> listImage){
        this.mContext=context;
        this.mListImageInfo=listImage;
        this.mInflater=LayoutInflater.from(context);
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_image_with_note, parent, false);
        return new ImageNoteAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        List<Integer> allImageID = DumpData.getAllImageLocal(mContext);

        final ImageInfo imageInfo = mListImageInfo.get(position);
        Picasso.with(mContext).load(allImageID.get(imageInfo.getmImageID()-1))
                .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                .skipMemoryCache()
                .resize(size, size)
                .centerInside()
                .into(holder.imageItem, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.textviewNote.setText(imageInfo.getNoteForImage());
                    }

                    @Override
                    public void onError() {

                        holder.imageItem.setImageResource(R.drawable.no_image);
                    }
                });
    }

    public void setListener(GridClickListener mListener) {
        this.mListener = mListener;
    }

    @Override
    public int getItemCount() {
        return mListImageInfo.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageItem;
        public TextView textviewNote;
        public MyViewHolder(final View itemView) {
            super(itemView);
            imageItem = (ImageView) itemView.findViewById(R.id.item_rec_image);
            textviewNote = (TextView) itemView.findViewById(R.id.item_rec_textview_note);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(itemView,getAdapterPosition());
                }
            });
        }
    }
}
