package nguyenson.com.inspirationcardfususu.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.model.PagerInfo;

/**
 * Created by nguyen on 17/04/2018.
 */

public class SplashPagerAdapter extends PagerAdapter {
    private Context mContext;
    private List<PagerInfo> mListPage;
    @Override
    public int getCount() {
        return mListPage.size();
    }

    public SplashPagerAdapter(List<PagerInfo> listPage, Context mContext) {
        this.mListPage = listPage;
        this.mContext = mContext;
    }
    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.pager_splash_item, container, false);
        container.addView(layout);

        ImageView imageViewSplash = (ImageView) layout.findViewById(R.id.splash_img_pager);
        TextView textViewDescription = (TextView) layout.findViewById(R.id.splash_tv_pager_description);

        imageViewSplash.setImageResource(mListPage.get(position).getImageResource());
        textViewDescription.setText(mListPage.get(position).getDescription());
        return layout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
