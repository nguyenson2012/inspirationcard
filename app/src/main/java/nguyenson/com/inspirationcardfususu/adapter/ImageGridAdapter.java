package nguyenson.com.inspirationcardfususu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.interf.GridClickListener;
import nguyenson.com.inspirationcardfususu.util.BitmapTransform;

/**
 * Created by Administrator on 03/09/2017.
 */

public class ImageGridAdapter extends RecyclerView.Adapter<ImageGridAdapter.MyViewHolder> {
    private LayoutInflater mInflater;
    private Context mContext;
    //
    private List<Integer> mListImage;

    private GridClickListener mListener;
    private static final int MAX_WIDTH = 1024;
    private static final int MAX_HEIGHT = 768;

    int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));

    public ImageGridAdapter(Context context, List<Integer> listImage){
        this.mContext=context;
        this.mListImage=listImage;
        this.mInflater=LayoutInflater.from(context);
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_recycler_grid_view, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Picasso.with(mContext).load(mListImage.get(position))
                .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                .skipMemoryCache()
                .resize(size, size)
                .centerInside()
                .placeholder(R.drawable.black).into(holder.imageQuote, new Callback() {
            @Override
            public void onSuccess() {
                holder.progressBarLoading.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                holder.progressBarLoading.setVisibility(View.GONE);
                holder.imageQuote.setImageResource(R.drawable.no_image);
            }
        });
    }

    public void setListener(GridClickListener mListener) {
        this.mListener = mListener;
    }

    @Override
    public int getItemCount() {
        return mListImage.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ImageView imageQuote;
        private ProgressBar progressBarLoading;
        public MyViewHolder(View itemView) {
            super(itemView);
            imageQuote=(ImageView)itemView.findViewById(R.id.image_in_grid);
            imageQuote.setOnClickListener(this);
            progressBarLoading=(ProgressBar)itemView.findViewById(R.id.progress_loading);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.image_in_grid:
                    mListener.onClick(v,getAdapterPosition());
                    break;
            }
        }
    }
}
