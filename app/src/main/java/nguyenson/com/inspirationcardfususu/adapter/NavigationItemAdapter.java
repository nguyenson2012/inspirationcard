package nguyenson.com.inspirationcardfususu.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.model.NavigationItem;

/**
 * Created by Administrator on 10/09/2017.
 */

public class NavigationItemAdapter extends ArrayAdapter<NavigationItem> {
    private Activity context;
    private List<NavigationItem> listItem;
    public NavigationItemAdapter(Activity context, int resource, List<NavigationItem> listItem) {
        super(context, resource, listItem);
        this.context=context;
        this.listItem=listItem;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            LayoutInflater inflater=context.getLayoutInflater();
            convertView=inflater.inflate(R.layout.item_navigation,parent,false);
        }
        NavigationItem navigationItem=listItem.get(position);
        MyViewHolder myViewHolder=new MyViewHolder(convertView);
        myViewHolder.mTextviewItem.setText(navigationItem.getTitle());
        myViewHolder.mImageItem.setImageResource(navigationItem.getDrawableItem());
        return convertView;
    }

    class MyViewHolder{
        public ImageView mImageItem;
        public TextView mTextviewItem;

        public MyViewHolder(View view){
            mImageItem=(ImageView)view.findViewById(R.id.image_icon_navigation);
            mTextviewItem=(TextView)view.findViewById(R.id.textview_item_title);
        }
    }
}
