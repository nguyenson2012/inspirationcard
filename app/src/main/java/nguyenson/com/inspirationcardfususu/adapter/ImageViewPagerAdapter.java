package nguyenson.com.inspirationcardfususu.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import nguyenson.com.inspirationcardfususu.R;
import nguyenson.com.inspirationcardfususu.util.BitmapTransform;
import nguyenson.com.inspirationcardfususu.util.CommonUtil;

/**
 * Created by Administrator on 09/09/2017.
 */

public class ImageViewPagerAdapter extends PagerAdapter {
    private List<Integer> listImage;
    private Context mContext;

    private static final int MAX_WIDTH = CommonUtil.getScreenWidth();
    private static final int MAX_HEIGHT = CommonUtil.getScreenHeight()/3;

    int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));

    public ImageViewPagerAdapter(List<Integer> listImage, Context mContext) {
        this.listImage = listImage;
        this.mContext = mContext;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_view_pager, container, false);
        container.addView(layout);
        final ImageView imageViewDetail=(ImageView)layout.findViewById(R.id.image_in_viewpager);
        final ProgressBar progressBar=(ProgressBar)layout.findViewById(R.id.progress_loading_in_view_pager);
        Picasso.with(mContext).load(listImage.get(position))
                .placeholder(R.drawable.black)
                .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                .skipMemoryCache()
                .resize(MAX_WIDTH, MAX_HEIGHT)
                .centerInside()
                .into(imageViewDetail, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);
                        imageViewDetail.setImageResource(R.drawable.no_image);
                    }
                });
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return listImage.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }
}
